﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oef_23._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        Random mijnRandom = new Random();
        public void btnGooien_Click(object sender, RoutedEventArgs e)
        {
            Dobbelsteen dobbelsteen1 = new Dobbelsteen(4, mijnRandom);
            dobbelsteen1.Roll();
            Dobbelsteen dobbelsteen2 = new Dobbelsteen();
            dobbelsteen2.Roll();
            txtRandomResultaatBlauw.Content = dobbelsteen1.Waarde.ToString();
            txtRandomResultaatRood.Content = dobbelsteen2.Waarde.ToString();
        }

    }
}
