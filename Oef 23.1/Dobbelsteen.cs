﻿using System;

namespace Oef_23._1
{
    class Dobbelsteen
    {
        //attributen
        private int _aantalZijden;
        private int _waarde;
        private Random _willGetal;

        //constructors
        public Dobbelsteen()
        {
            AantalZijden = 6;
        }
        public Dobbelsteen(int aantalZijden, Random r)
        {
            AantalZijden = aantalZijden;
            willGetal = r;
        }
        public Dobbelsteen(Random r)
        {
            r = willGetal;
        }

        //properties
        public int AantalZijden
        {
            get { return _aantalZijden; }
            set { _aantalZijden = value; }
        }
        public int Waarde
        {
            get { return _waarde; }
            set { _waarde = value; }
        }
        public Random willGetal
        {
            get { return _willGetal; }
            set { _willGetal = value; }
        }

        //methodes
        public void Roll()
        {
            willGetal = new Random();
            Waarde = willGetal.Next(1, AantalZijden + 1);
        }
    }
}
